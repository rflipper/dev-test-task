﻿using json_api_test.DTO;
using Microsoft.AspNetCore.Mvc;

namespace json_api_test.Controllers
{
    [ApiController]
    [Route("api/test/[action]")]
    public class TestController : ControllerBase
    {
        private readonly IStorage _storage;

        public TestController(IStorage storage)
        {
            _storage = storage;
        }

        [RequestSizeLimit(1_000_000_000)]
        [HttpPost]
        public IActionResult Upload([FromBody]UploadRequest request)
        {
            var name = request.Name;
            
            var content = _storage.Upload(request.File, request.Name);

            var json = System.Text.Encoding.UTF8.GetBytes(request.Json.ToString());
            var data = _storage.Upload(json, ".json");

            return Ok(new
            {
                name,
                data,
                content
            });
        }
    }
}
