﻿namespace json_api_test
{
    public interface IStorage
    {
        public string Upload(byte[] data, string name);
    }

    public class StorageService : IStorage
    {
        public string Upload(byte[] data, string name)
        {
            var file = Path.GetTempFileName();
            File.WriteAllBytes(file, data);

            return file;
        }
    }
}
